﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recursive_Quicksort
{
    public partial class Form1 : Form
    {

        // { 5, 2, 8, 4, 1, 10, 7, 3, 9, 6 }; This is the array that needs to be sorted.
        //First Iteration: {2, 5, 8, 4, 1, 10, 7, 3, 9, 6}
        int[] SortInteger = { 5, 2, 8, 4, 1, 10, 7, 3, 9, 6 };

        public Form1()
        {
            InitializeComponent();

        }

        public void ShowIntegerArray(int[] sortie, int index)
        {
            //     "Sortie " means "exit" in French. So, I chose that for the name of the index
            //     because the user can exit without sorting the array.
            if (index < sortie.Length)
            {
                listBox1.Items.Add($"{sortie[index]} ");
                ShowIntegerArray(sortie, index + 1);
            }
            else listBox1.Items.Add(" Would you like to sort this array? ");
        }


        private void QuickSort(int[] array, int left, int right)
        {
            int l = left, r = right;
            int pivot = array[(left + right) / 2];

            while (l <= r)
            {
                while (array[l].CompareTo(pivot) < 0)
                {
                    l++;
                }

                while (array[r].CompareTo(pivot) > 0)
                {
                    r--;
                }

                if (l <= r)
                {
                    // Swap
                    Swap(ref array, l, r);

                    l++;
                    r--;
                }
            }

            if (left < r)
            {
                QuickSort(array, left, r);
            }

            if (l < right)
            {
                QuickSort(array, l, right);
            }
        }

        private void Swap(ref int[] array, int l, int r)
        {
            int tmp = array[l];
            array[l] = array[r];
            array[r] = tmp;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ShowIntegerArray(SortInteger, 0);
        }

        private void ButtonNo_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            this.Show();
        }

        private void ButtonYes_Click(object sender, EventArgs e)
        {
            QuickSort(SortInteger, 0, SortInteger.Length - 1);
            ShowIntegerArray(SortInteger, 0);
        }
    }
}
